import HelloWorld from "./components/HelloWorld.vue";
import ToDo from "./components/todo.vue";
import Summary from "./components/summary.vue";
import taskDetail from "./components/taskDetail.vue";

export default [
  { path:'/', component: HelloWorld},
  { path:'/todo', component: ToDo},
  { path:'/summary', component: Summary},
  { path:'/taskDetail', component: taskDetail},
   ];