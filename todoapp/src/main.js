import Vue from "vue";
import App from "./App.vue";
import vueRouter from "vue-router";
import Routes from "./routes";
import axios from "axios";
import VueAxios from "vue-axios";
import VModal from 'vue-js-modal';

//  To use api we use Vue-axios..
//  npm install --save axios vue-axios... 
Vue.use( VueAxios, axios);
Vue.use(VModal)
Vue.prototype.$axios = axios;
Vue.use(vueRouter);
Vue.config.productionTip = false;


const router =  new vueRouter({
  // creating a instance of routing...
  routes: Routes,
  mode: "history"
  });

new Vue({

  router:router,
  render: h => h(App),
}).$mount("#app");
