-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2019 at 12:34 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo_vue`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `Id` int(11) NOT NULL,
  `task_type` varchar(255) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `task_location` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `task_time` date NOT NULL,
  `is_all_day` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`Id`, `task_type`, `task_name`, `task_location`, `file_name`, `task_time`, `is_all_day`, `status`, `created_at`) VALUES
(1, 'Business', '67676', '676767', 'download-(1).jpg', '2019-11-13', 1, 1, '2019-11-07 10:35:06'),
(2, 'Business', '67676', '676767', 'download-(1).jpg', '2019-11-13', 1, 1, '2019-11-07 10:38:02'),
(3, 'Personal', 'TTT', 'NNNN', '11.jpg', '2019-11-13', 1, 1, '2019-11-07 11:19:36'),
(4, '', '', '', '', '0000-00-00', 0, 1, '2019-11-07 11:26:36'),
(5, 'Personal', 'TTTT', 'NNNN', 'ccf016d43b5a019d4d0ae7f0f8fccc36.jpg', '2019-11-18', 1, 1, '2019-11-07 11:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `task_type`
--

CREATE TABLE `task_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_type`
--

INSERT INTO `task_type` (`id`, `title`, `count`, `status`) VALUES
(1, 'All Things', 0, 1),
(2, 'Business', 2, 1),
(3, 'Personal', 2, 1),
(4, 'Family', 0, 1),
(5, 'Work', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `task_type`
--
ALTER TABLE `task_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `task_type`
--
ALTER TABLE `task_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table tasks
--

--
-- Metadata for table task_type
--

--
-- Metadata for database todo_vue
--
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
